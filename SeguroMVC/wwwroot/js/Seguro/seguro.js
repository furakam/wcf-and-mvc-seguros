import { listarSeguros, fillComboCompanias } from "./listar.js";
import { enviarDatos } from "./transacciones.js";


//Limpiar datos del formulario
function clearData() {
  const elements = $(".clear-data")
  elements.val("");
  elements.removeClass("error")

}

//Funciones Globales

window.abrirModal = function(id){
  clearData();
  $('#modal-form').validate().resetForm()
  if (id !== undefined) {
    $.get(
      `Seguro/GetSeguroById/${id}`,
      function (data) {
        $.each(data.data, function (key, value) {
          $(`#modal-form #${key}`).val(value)
        });
      }
    );
  }
};

window.eliminar = function(id) {
  if (id !== undefined) {
    if (confirm("Estas seguro de eliminar el registro?")) {
      $.ajax({
        url: `/Seguro/DeleteSeguroById/${id}`,
        type: "DELETE",
        processData: false,
        contentType: false,
        success: function (datos) {
          if (datos.success) {
            listarSeguros();
          } else {
            alert(datos.message);
          }
        },
      });
    }
  }
}


//Listando los seguros
listarSeguros()

//Llenando el combo de compannias del formulario de registro
fillComboCompanias()

// Asignando el evento de enviar datos al elemento btnEnviarDatos
$("#btnEnviarDatos").click(enviarDatos);

