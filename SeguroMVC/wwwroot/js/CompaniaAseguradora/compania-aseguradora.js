import { enviarDatos } from "./transacciones.js";
import { listarCompaniaSeguro } from "./listar.js";

function clearData() {
  const elements = $(".clear-data")
  elements.val("");
  elements.removeClass("error")

}

window.abrirModal = function (id) {
  clearData();
  $('#modal-form').validate().resetForm()
  if (id !== undefined) {
    $.get(
      `CompaniaAseguradora/GetCompaniaAseguradoraById/${id}`,
      function (data) {
        $.each(data.data, function (key, value) {
          $(`#modal-form #${key}`).val(value)
        });
      }
    );
  }
};

window.eliminar = function(id) {
  if (id !== undefined) {
    if (confirm("Estas seguro de eliminar el registro?")) {
      $.ajax({
        url: `/CompaniaAseguradora/DeleteCompaniaAseguradoraById/${id}`,
        type: "DELETE",
        processData: false,
        contentType: false,
        success: function (datos) {
          if (datos.success) {
            listarCompaniaSeguro();
          } else {
            alert(datos.message);
          }
        },
      });
    }
  }
}



listarCompaniaSeguro()

// Asignando el evento de enviar datos al elemento btnEnviarDatos
$("#btnEnviarDatos").click(enviarDatos);