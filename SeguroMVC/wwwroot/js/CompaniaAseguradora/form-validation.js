$(document).ready(function () {
  $("#modal-form").validate({
    rules: {
      ccompaniarazonsocial: {
        required: true,
      },
      ccompaniadescripcion: {
        required: true,
      },
      ccompaniaruc: {
        minlength: 11,
        maxlength: 11,
        required: true,
      },
      ccompaniacontacto: {
        minlength: 9,
        maxlength: 9,
        required: true,
      },
      ccompaniacelular: {
        minlength: 9,
        maxlength: 9,
        required: true,
      },
      dcompaniafecharenovacion: {
        required: true,
      },
    },
    messages: {
      ccompaniarazonsocial: {
        required: "Este campo es requerido",
      },
      ccompaniadescripcion: {
        required: "Este campo es requerido",
      },
      ccompaniaruc: {
        minlength: "El ruc debe tener como mínimo 11 caracteres",
        maxlength: "El ruc debe tener como máximo 11 caracteres",
        required: "Este campo es requerido",
      },
      ccompaniacontacto: {
        minlength: "El contacto debe tener como mínimo 9 caracteres",
        maxlength: "El contacto debe tener como máximo 9 caracteres",
        required: "Este campo es requerido",
      },
      ccompaniacelular: {
        minlength: "El celular debe tener como mínimo 9 caracteres",
        maxlength: "El celular debe tener como máximo 9 caracteres",
        required: "Este campo es requerido",
      },
      dcompaniafecharenovacion: {
        required: "Este campo es requerido",
      },
    },
  });
});
