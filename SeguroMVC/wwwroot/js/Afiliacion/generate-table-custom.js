/**
 * Esta funcion crea una tabla en base a las columnas y datos. Posteriormente los coloca dentro del elemento mediante el id del mismo.
 * @param {string[]} columns
 * @param {Object[]} data
 * @param {string} elementId
 */
export function generateTable(columns, data, elementId) {
  let headers = "";
  columns.forEach((header) => {
    headers += `<th class="bg-primary text-white fw-normal text-center">${header}</th>`;
  });

  let rows = "";
  data.forEach((data) => {
    let cols = "";
    let id = "";
    Object.entries(data).forEach((entrie) => {
      if (id == "") id = entrie[1];

      if (entrie[0] !== "cronograma") {
        cols += `<td class="text-truncate text-center" style="max-width: 350px;min-width: 150px">${entrie[1]}</td>`;
      } else {
        cols += `
          <td>
            <div class="d-flex gap-2">
              ${
                entrie[1]
                  ? `<button type="button" onclick="verCronograma(${id})" data-bs-toggle="modal" data-bs-target="#cronogramaModal"  class="btn btn-dark w-100" id="btnVerCronograma">Ver</button>`
                  : `<button type="button" onclick="generarCronograma(${id})" class="btn btn-info w-100" id="btnGenerarCronograma">Generar</button>`
              }
            </div>
          </td>
        `;
      }
    });
    cols += `
    <td>
      <div class="d-flex gap-2">
        <button type="button" onclick="abrirModal(${id})" data-bs-toggle="modal" data-bs-target="#registerModal" class="btn btn-warning" id="btnEditar">Editar</button>
        <button class="btn btn-danger" id="btnEliminar" onclick="eliminar(${id})">Eliminar</button>
      </div>
    </td>
    `;
    rows += `<tr>${cols}</tr>`;
  });

  let htmlTable = `
  <table class="table table-light">
    <thead>
      <tr>
        ${headers}
      </tr>
    </thead>
    <tbody>
      ${rows}
    </tbody>
  </table>
  `;
  $(`#${elementId}`).html(htmlTable);
}

export function generateTableCronograma(columns, data, elementId) {
  let headers = "";
  columns.forEach((header) => {
    headers += `<th class="bg-primary text-white fw-normal text-center">${header}</th>`;
  });

  let rows = "";
  data.forEach((data) => {
    let cols = "";
    Object.entries(data).forEach((entrie) => {
      cols += `<td class="text-truncate text-center" style="max-width: 350px;min-width: 150px">${entrie[1]}</td>`;
    });
    rows += `<tr>${cols}</tr>`;
  });

  let htmlTable = `
  <table class="table table-light">
    <thead>
      <tr>
        ${headers}
      </tr>
    </thead>
    <tbody>
      ${rows}
    </tbody>
  </table>
  `;
  $(`#${elementId}`).html(htmlTable);
}
