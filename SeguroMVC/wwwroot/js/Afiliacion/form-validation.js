$(document).ready(function () {
  $("#modal-form").validate({
    rules: {
      nclienteid: {
        required: true,
      },
      nseguroid: {
        required: true,
      }
    },
    messages: {
      nclienteid: {
        required: "Este campo es requerido",
      },
      nseguroid: {
        required: "Este campo es requerido",
      }
    },
  });
});