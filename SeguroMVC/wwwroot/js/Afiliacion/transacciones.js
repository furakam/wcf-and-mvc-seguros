import { listarAfiliaciones } from "./listar.js";

export function enviarDatos() {
  //Comprobando que el formulario contenga los datos válidos
  var isValidate = $("#modal-form").valid();

  if (isValidate) {
    const id = $("#nafiliacionid").val();

    //Obteniendo todos los datos del formulario
    const dataform = new FormData($("#modal-form")[0]);
    if (id === undefined || id === "") {
      $.ajax({
        url: "/Afiliacion/RegistrarAfiliacion",
        type: "POST",
        data: dataform,
        processData: false,
        contentType: false,
        success: function (response) {
          if (response.success) {
            $("#btnCloseModal").trigger("click"); //Haciendo click en el botón de cerrar modal
            listarAfiliaciones();
          }
        },
        error: function({responseJSON}, status, error){
          alert(responseJSON.message)
        }
      });
    } else {
      console.log("Actualizando")
      $.ajax({
        url: "/Afiliacion/ActualizarAfiliacion",
        type: "PUT",
        data: dataform,
        processData: false,
        contentType: false,
        success: function (datos) {
          if (datos.success) {
            $("#btnCloseModal").trigger("click"); //Haciendo click en el botón de cerrar modal
            listarAfiliaciones();
          }
        },
        error: function({responseJSON}, status, error){
          alert(responseJSON.message)
        }
      });
    }
  }
}


