// Contenedor donde se renderizarán todos los links
const linkContainer = $("#link-container");

//Lista de links que se renderizarán en el navbar
const listaLinks = [
  {
    route: 'CompaniaAseguradora',
    label: 'Compañías aseguradoras',
  },
  {
    route: 'Cliente',
    label: 'Clientes',
  },
  {
    route: 'Seguro',
    label: 'Seguros',
  },
  {
    route: 'Afiliacion',
    label: 'Afiliaciones'
  }
];

//Variable que contendra el html a insertar en el contenedor linkContainer
let contenidoLinks = "";

//Declaración de funciones
/**
 * Esta función recorre la lista de objetos de la variable listaLinks, extrayendo la clave y el valor que posterior seran colocados dentro del elemento a tomando la clave como href e id y el valor como el label
 */
function setLinksInNavbar() {
  listaLinks.forEach((link) => {
    contenidoLinks += `
    <li class="nav-item">
      <a class="nav-link" id="${link.route}" href="/${link.route}">${link.label}</a>
    </li>
  `;
  });

  linkContainer.html(contenidoLinks);
}


/**
 * Esta funcion permite que cuando estemos en una determinada página el navlink de la página respectiva se active
 */
function activeLinkFunction() {
  $(document).ready(function () {
    let pathname = window.location.pathname.substring(1);
    linkContainer.children().removeClass("active");
    $(`#${pathname}`).addClass("active");
  });
}

//Ejecutando funciones
setLinksInNavbar();
activeLinkFunction();
