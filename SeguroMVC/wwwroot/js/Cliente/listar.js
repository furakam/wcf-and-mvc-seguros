import { generateTable } from "../generate-table.js";

/**
 * Esta funcion realiza el listado de datos en la tabla de Clientes
 */
export function listarClientes(){
  //Lista de columnas
  const columns = [
    "Id",
    "Nomberes",
    "Apellido paterno",
    "Apellido materno",
    "Documento",
    "Email",
    "Telefono",
    "Genero",
    "Acciones",
  ];
  $.get("/Cliente/ListarClientes", function (data) {
    generateTable(columns, data, "table");
  });
}