using CompaniaAseguradoraServiceReference;
using Microsoft.AspNetCore.Mvc;
using SeguroMVC.Models;
namespace SeguroMVC.Controllers
{
    [Route("[controller]")]
    public class CompaniaAseguradoraController : Controller
    {
        private readonly TMCompaniaAseguradoraServiceClient service = new ();

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("ListarCompaniaAseguradora")]
        public async Task<ActionResult<IEnumerable<GetTmcompaniaAseguradoraDto>>> ListarCompaniaAseguradora()
        {
            var response = Enumerable.Empty<GetTmcompaniaAseguradoraDto>();
            try
            {
                var companias = await service.ListarCompaniaAseguradoraAsync();
                response = companias.Data;
                return Ok(response);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest(response);
            }
        }

        [HttpPost("RegistrarCompaniaAseguradora")]
        public async Task<ActionResult<Response<string>>> RegistrarCompaniaAseguradora(CreateTmcompaniaAseguradoraDto companiaForm)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.RegistrarCompaniaAseguradoraAsync(companiaForm);
                response.Data = serviceResponse.Data ?? serviceResponse.Message;
            }
            catch (Exception error)
            {
                response.Message = error.Message;
                response.Success = false;
                return BadRequest(response);
            }
            return response;
        }

        [HttpPut("ActualizarCompaniaAseguradora")]
        public async Task<ActionResult<Response<string>>> ActualizarCompaniaAseguradora(UpdateTmcompaniaAseguradoraDto companiaForm)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.ActualizarCompaniaAseguradoraAsync(companiaForm);
                response.Data = serviceResponse.Data ?? serviceResponse.Message;
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }
            return response;
        }

        [HttpGet("GetCompaniaAseguradoraById/{id}")]
        public async Task<ActionResult<Response<GetFullTmcompaniaAseguradoraDto>>> GetCompaniaAseguradoraById(int id)
        {
            var response = new Response<GetFullTmcompaniaAseguradoraDto>();
            try
            {
                var serviceResponse = await service.GetCompaiaAseguradoraByIdAsync(id);
                response.Data = serviceResponse.Data;
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }
            return response;
        }

        [HttpDelete("DeleteCompaniaAseguradoraById/{id}")]
        public async Task<ActionResult<Response<string>>> DeleteCompaniaAseguradoraById(int id)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.DeleteCompaiaAseguradoraByIdAsync(id);
                response.Message = serviceResponse.Data?? serviceResponse.Message;
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }
            return response;
        }
    }
}