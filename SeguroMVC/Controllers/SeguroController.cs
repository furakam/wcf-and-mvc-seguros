using Microsoft.AspNetCore.Mvc;
using SeguroMVC.Models;
using SeguroServiceReference;

namespace SeguroMVC.Controllers
{
    [Route("[controller]")]
    public class SeguroController : Controller
    {
        private readonly SeguroServiceClient service = new();

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("GetSeguroById/{id}")]
        public async Task<ActionResult<Response<GetFullTmsegurodto>>> GetSeguroById(int id)
        {
            var response = new Response<GetFullTmsegurodto>();
            try
            {
                var seguro = await service.GetSeguroByIdAsync(id);

                response.Data = seguro.Data ?? throw new Exception(seguro.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }

            return response;
        }

        [HttpGet("ListarSeguros")]
        public async Task<ActionResult<Response<IEnumerable<GetTmseguroDto>>>> ListarSeguros()
        {
            var response = new Response<IEnumerable<GetTmseguroDto>>();

            try
            {
                var serviceResponse = await service.ListarSeguroAsync();

                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
            }

            return response;
        }

        [HttpPost("RegistrarSeguro")]
        public async Task<ActionResult<Response<string>>> RegistrarSeguro(CreateTmseguroDto seguroForm)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.RegistrarSeguroAsync(seguroForm);
                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Message = error.Message;
                response.Success = false;
                return BadRequest(response);
            }
            return response;
        }

        [HttpPut("ActualizarSeguro")]
        public async Task<ActionResult<Response<string>>> ActualizarSeguro(UpdateTmseguroDto seguroForm)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.ActualizarSeguroAsync(seguroForm);
                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }
            return response;
        }

        [HttpDelete("DeleteSeguroById/{id}")]
        public async Task<ActionResult<Response<string>>> DeleteSeguroById(int id)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.EliminarSeguroAsync(id);
                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message=error.Message;
                return NotFound(response);
            }
            return response;
        }

    }
}