using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SeguroMVC.Models;
using Microsoft.EntityFrameworkCore;
using AfiliacionServiceReference;

namespace SeguroMVC.Controllers
{
    [Route("[controller]")]
    public class AfiliacionController : Controller
    {
        private readonly AfiliacionServiceClient service = new();

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("ListarAfiliaciones")]
        public async Task<ActionResult<Response<IEnumerable<GetTrafiliacion>>>> ListarAfiliaciones()
        {
            var response = new Response<IEnumerable<GetTrafiliacion>>();

            try
            {
                var serviceResponse = await service.ListarAfiliacionAsync();

                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
            }

            return response;
        }

        [HttpGet("GetAfiliacionById/{id}")]
        public async Task<ActionResult<Response<GetFullTrafiliacion>>> GetAfiliacionById(int id)
        {
            var response = new Response<GetFullTrafiliacion>();
            try
            {
                var serviceResponse = await service.GetAfiliacionByIdAsync(id);

                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }

            return response;
        }

        [HttpPost("RegistrarAfiliacion")]
        public async Task<ActionResult<Response<string>>> RegistrarAfiliacion(CreateTrafiliacion afiliacionForm)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.RegistrarAfiliacionAsync(afiliacionForm);

                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Message = error.Message;
                response.Success = false;
                return BadRequest(response);
            }
            return response;
        }

        [HttpPut("ActualizarAfiliacion")]
        public async Task<ActionResult<Response<string>>> ActualizarAfiliacion(UpdateTrafiliacionDto afiliacionForm)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.ActualizarAfiliacionAsync(afiliacionForm);

                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }
            return response;
        }

        [HttpDelete("DeleteAfiliacionById/{id}")]
        public async Task<ActionResult<Response<string>>> DeleteAfiliacionById(int id)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.EliminarAfiliacionAsync(id);
                response.Message = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }
            return response;
        }


    }
}