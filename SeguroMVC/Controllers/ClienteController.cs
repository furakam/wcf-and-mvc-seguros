using ClienteServiceReference;
using Microsoft.AspNetCore.Mvc;
using SeguroMVC.Models;

namespace SeguroMVC.Controllers
{
    [Route("[controller]")]
    public class ClienteController : Controller
    {
        private readonly ClienteServiceClient service = new();

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("ListarClientes")]
        public async Task<ActionResult<IEnumerable<GetTmclienteDto>>> ListarClientes()
        {
            var response = Enumerable.Empty<GetTmclienteDto>();

            try
            {
                var clientes = await service.ListarClientesAsync();
                response = clientes.Data;
                return Ok(response);
            }
            catch (Exception)
            {
                return BadRequest(response);
            }
        }

        [HttpGet("GetClienteById/{id}")]
        public async Task<ActionResult<Response<GetFullTmclienteDto>>> GetClienteById(int id)
        {
            var response = new Response<GetFullTmclienteDto>();
            try
            {
                var serviceResponse = await service.GetClienteByIdAsync(id);

                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }

            return response;
        }

        [HttpPost("RegistrarCliente")]
        public async Task<ActionResult<Response<string>>> RegistrarCliente(CreateTmclienteDto clienteForm)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.RegistrarClienteAsync(clienteForm);
                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Message = error.Message;
                response.Success = false;
                return BadRequest(response);
            }
            return response;
        }

        [HttpPut("ActualizarCliente")]
        public async Task<ActionResult<Response<string>>> ActualizarCliente(UpdateTmclienteDto clienteForm)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.ActualizarClienteAsync(clienteForm);

                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
                return NotFound(response);
            }
            return response;
        }

        [HttpDelete("DeleteClienteById/{id}")]
        public async Task<ActionResult<Response<string>>> DeleteClienteById(int id)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.EliminarClienteAsync(id);
                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message=error.Message;
                return NotFound(response);
            }
            return response;
        }

    }
}