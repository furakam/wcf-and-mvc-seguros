using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using SeguroMVC.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PagoServiceReference;

namespace SeguroMVC.Controllers
{
    [Route("[controller]")]
    public class PagoController : Controller
    {
        private readonly PagoServiceClient service = new();

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("ListarCronograma/{id}")]
        public async Task<ActionResult<Response<IEnumerable<GetTRPagoDto>>>> ListarCronograma(int id)
        {
            var response = new Response<IEnumerable<GetTRPagoDto>>();
            try
            {
                var serviceResponse = await service.GetPagosByAfiliacionIdAsync(id);
                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
            }

            return Ok(response);
        }

        [HttpGet("GenerarCronograma/{id}")]
        public async Task<ActionResult<Response<string>>>  GenerarCronograma(int id)
        {
            var response = new Response<string>();
            try
            {
                var serviceResponse = await service.GenerarCronogramaPagoAsync(id);
                response.Data = serviceResponse.Data ?? throw new Exception(serviceResponse.Message);
            }
            catch (Exception error)
            {
                response.Success = false;
                response.Message = error.Message;
            }

            return Ok(response);
        }
    }
}