//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WFCSeguro.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class TMSeguro
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TMSeguro()
        {
            this.cseguroestado = true;
            this.TRAfiliacion = new HashSet<TRAfiliacion>();
        }
    
        public int nseguroid { get; set; }
        public int ncompaniaid { get; set; }
        public string cseguronumero { get; set; }
        public string ntiposeguro { get; set; }
        public string csegurodescripcion { get; set; }
        public Nullable<int> nsegurofactorimpuesto { get; set; }
        public int nseguroporcentajecomision { get; set; }
        public decimal nseguromontoprima { get; set; }
        public string nmonedaid { get; set; }
        public int nseguroedadmaxima { get; set; }
        public System.DateTime dsegurofechavigencia { get; set; }
        public decimal nseguroimportemensual { get; set; }
        public decimal nsegurocobertura { get; set; }
        public Nullable<bool> cseguroestado { get; set; }
        public string cusercreate { get; set; }
        public Nullable<System.DateTime> dusercreate { get; set; }
        public string cuserupdate { get; set; }
        public Nullable<System.DateTime> duserupdate { get; set; }
    
        public virtual TMCompaniaAseguradora TMCompaniaAseguradora { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TRAfiliacion> TRAfiliacion { get; set; }
    }
}
