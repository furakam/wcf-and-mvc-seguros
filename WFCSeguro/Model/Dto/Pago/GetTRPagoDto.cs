﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WFCSeguro.Model.Dto.Pago
{
    [DataContract]
    public class GetTRPagoDto
    {
        [DataMember(Order = 1)]
        public int NpagoId { get; set; }

        [DataMember(Order = 2)]
        public int Nafiliacionid { get; set; }

        [DataMember(Order = 3)]
        public string Dpagofecha { get; set; }

        [DataMember(Order = 4)]
        public decimal Npagocuota { get; set; }

        [DataMember(Order = 5)]
        public string Cpagoestado { get; set; }
    }
}