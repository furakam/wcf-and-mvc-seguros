
using System.Runtime.Serialization;

namespace WFCSeguro.Models.Dto.Afiliacion
{
    [DataContract]
    public class GetTrafiliacion
    {
        [DataMember(Order = 1)]
        public int Nafiliacionid { get; set; }

        [DataMember(Order = 2)]
        public string Cclientedocumentoidentidad { get; set; }

        [DataMember(Order = 3)]
        public string Cseguronumero { get; set; }

        [DataMember(Order = 4)]
        public string Dsegurofechaafiliación { get; set; }

        [DataMember(Order = 5)]
        public bool Cronograma { get; set; }
    }
}