
using System.Runtime.Serialization;

namespace WFCSeguro.Models.Dto.Cliente
{
    [DataContract]
    public class GetTmclienteDto
    {
        [DataMember(Order = 1)]
        public int Nclienteid { get; set; }

        [DataMember(Order = 2)]
        public string Cclientenombres { get; set; }

        [DataMember(Order = 3)]
        public string Cclienteapellidopaterno { get; set; }

        [DataMember(Order = 4)]
        public string Cclienteapellidomaterno { get; set; }

        [DataMember(Order = 5)]
        public string Cclientedocumentoidentidad { get; set; }

        [DataMember(Order = 6)]
        public string Cclienteemail { get; set; }

        [DataMember(Order = 7)]
        public string Cclientetelefono { get; set; }

        [DataMember(Order = 8)]
        public string Cclientegenero { get; set; }
    }
}