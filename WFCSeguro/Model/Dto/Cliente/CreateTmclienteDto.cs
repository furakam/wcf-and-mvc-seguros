using System;
using System.Collections.Generic;

namespace WFCSeguro.Models.Dto.Cliente
{
    public class CreateTmclienteDto
    {
        public string Cclientenombres { get; set; }
        public string Cclienteapellidopaterno { get; set; }
        public string Cclienteapellidomaterno { get; set; }
        public string Ntipodocumento { get; set; }
        public string Cclientedocumentoidentidad { get; set; }
        public string Cclientegenero { get; set; }
        public string Cclienteemail { get; set; }
        public string Cclientetelefono { get; set; }
        public string Cclientedireccion { get; set; }
        public DateTime Dclientefechanacimiento { get; set; }
    }
}