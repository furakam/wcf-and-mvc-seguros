namespace WFCSeguro.Models.Dto.Cliente
{
    public class GetFullTmclienteDto
    {
        public int Nclienteid { get; set; }

        public string Ntipodocumento { get; set; }

        public string Cclientedocumentoidentidad { get; set; }

        public string Cclientegenero { get; set; }

        public string Cclienteapellidopaterno { get; set; }

        public string Cclienteapellidomaterno { get; set; }

        public string Cclientenombres { get; set; }

        public string Cclienteemail { get; set; }

        public string Cclientetelefono { get; set; }

        public string Cclientedireccion { get; set; }

        public string Dclientefechanacimiento { get; set; } = string.Empty;
    }
}