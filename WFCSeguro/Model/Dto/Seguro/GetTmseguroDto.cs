using System.Runtime.Serialization;

namespace WFCSeguro.Models.Dto.Seguro
{
    [DataContract]
    public class GetTmseguroDto
    {
        [DataMember(Order = 1)]
        public int Nseguroid { get; set; }

        [DataMember(Order = 2)]
        public string Ccompaniarazonsocial { get; set; }

        [DataMember(Order = 3)]
        public string Cseguronumero { get; set; }

        [DataMember(Order = 4)]
        public string Ntiposeguro { get; set; }

        [DataMember(Order = 5)]
        public string Csegurodescripcion { get; set; }

        [DataMember(Order = 6)]
        public int? Nsegurofactorimpuesto { get; set; }

        [DataMember(Order = 6)]
        public int Nseguroporcentajecomision { get; set; }

        [DataMember(Order = 7)]
        public decimal Nseguromontoprima { get; set; }

        [DataMember(Order = 8)]
        public string Nmonedaid { get; set; }

        [DataMember(Order = 9)]
        public int Nseguroedadmaxima { get; set; }

        [DataMember(Order = 10)]
        public string Dsegurofechavigencia { get; set; } = string.Empty;

        [DataMember(Order = 11)]
        public decimal Nseguroimportemensual { get; set; }

        [DataMember(Order = 12)]
        public decimal Nsegurocobertura { get; set; }
    }
}