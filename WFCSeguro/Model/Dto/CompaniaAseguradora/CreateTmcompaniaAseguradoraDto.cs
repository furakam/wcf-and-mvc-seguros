using System;

namespace WFCSeguro.Models.Dto.CompaniaAseguradora
{
    public class CreateTmcompaniaAseguradoraDto
    {
        public string Ccompaniarazonsocial { get; set; }
        public string Ccompaniadescripcion { get; set; }
        public string Ccompaniaruc { get; set; }
        public string Ccompaniacontacto { get; set; }
        public string Ccompaniacelular { get; set; }
        public DateTime Dcompaniafecharenovacion { get; set; }
    }
}