using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WFCSeguro.Models.Dto.CompaniaAseguradora
{
    public class GetFullTmcompaniaAseguradoraDto
    {
        public int Ncompaniaid { get; set; }
        public string Ccompaniarazonsocial { get; set; }
        public string Ccompaniadescripcion { get; set; }
        public string Ccompaniaruc { get; set; }
        public string Ccompaniacontacto { get; set; }
        public string Ccompaniacelular { get; set; }
        public string Dcompaniafecharenovacion { get; set; } = string.Empty;
    }
}