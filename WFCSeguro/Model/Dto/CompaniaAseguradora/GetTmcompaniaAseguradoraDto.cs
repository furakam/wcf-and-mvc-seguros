
using System.Runtime.Serialization;

namespace WFCSeguro.Models.Dto.CompaniaAseguradora
{
    [DataContract]
    public class GetTmcompaniaAseguradoraDto
    {

        [DataMember(Order = 1)]
        public int Ncompaniaid { get; set; }

        [DataMember(Order = 2)]
        public string Ccompaniadescripcion { get; set; }

        [DataMember(Order = 3)]
        public string Ccompaniarazonsocial { get; set; }

        [DataMember(Order = 4)]
        public string Ccompaniacontacto { get; set; }

        [DataMember(Order = 5)]
        public string Ccompaniacelular { get; set; }

        [DataMember(Order = 6)]
        public string Dcompaniafecharenovacion { get; set; } = string.Empty;

    }
}