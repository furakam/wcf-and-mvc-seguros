﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WFCSeguro.Configs.AutoMapper
{
    public class AutoMapperConfig
    {
        private static readonly Lazy<IMapper> lazyMapper = new Lazy<IMapper>(() =>
        {
            Mapper.Initialize(cfg => cfg.AddProfile<AutoMapperProfile>());
            return Mapper.Instance;
        });

        public static IMapper GetMapper()
        {
            return lazyMapper.Value;
        }
    }
}