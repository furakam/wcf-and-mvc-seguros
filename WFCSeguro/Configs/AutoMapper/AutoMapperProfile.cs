﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFCSeguro.Model;
using WFCSeguro.Model.Dto.Pago;
using WFCSeguro.Models.Dto.Afiliacion;
using WFCSeguro.Models.Dto.Cliente;
using WFCSeguro.Models.Dto.CompaniaAseguradora;
using WFCSeguro.Models.Dto.Seguro;

namespace WFCSeguro.Configs.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            //CompaniaAseguradora
            CreateMap<TMCompaniaAseguradora, GetTmcompaniaAseguradoraDto>().ForMember(dest => dest.Dcompaniafecharenovacion, opt => opt.MapFrom(src => src.dcompaniafecharenovacion.ToString("dd/MM/yyyy")));

            CreateMap<TMCompaniaAseguradora, GetFullTmcompaniaAseguradoraDto>().ForMember(dest => dest.Dcompaniafecharenovacion, opt => opt.MapFrom(src => src.dcompaniafecharenovacion.ToString("yyyy-MM-dd")));

            CreateMap<CreateTmcompaniaAseguradoraDto, TMCompaniaAseguradora>();

            //Cliente
            CreateMap<TMCliente, GetTmclienteDto>();
            CreateMap<TMCliente, GetFullTmclienteDto>().ForMember(dest => dest.Dclientefechanacimiento, opt => opt.MapFrom(src => src.dclientefechanacimiento.ToString("yyyy-MM-dd")));
            CreateMap<CreateTmclienteDto, TMCliente>();

            //Seguro
            CreateMap<TMSeguro, GetTmseguroDto>()
                .ForMember(dest => dest.Nseguromontoprima, opt => opt.MapFrom(src => Math.Round(src.nseguromontoprima, 2)))
                .ForMember(dest => dest.Nseguroimportemensual, opt => opt.MapFrom(src => Math.Round(src.nseguroimportemensual, 2)))
                .ForMember(dest => dest.Nsegurocobertura, opt => opt.MapFrom(src => Math.Round(src.nsegurocobertura, 2)))
                .ForMember(dest => dest.Ccompaniarazonsocial, opt => opt.MapFrom(src => src.TMCompaniaAseguradora.ccompaniarazonsocial))
                .ForMember(dest => dest.Dsegurofechavigencia, opt => opt.MapFrom(src => src.dsegurofechavigencia.ToString("dd/MM/yyyy")));

            CreateMap<TMSeguro, GetFullTmsegurodto>()
                .ForMember(dest => dest.Nseguromontoprima, opt => opt.MapFrom(src => Math.Round(src.nseguromontoprima, 2)))
                .ForMember(dest => dest.Nseguroimportemensual, opt => opt.MapFrom(src => Math.Round(src.nseguroimportemensual, 2)))
                .ForMember(dest => dest.Nsegurocobertura, opt => opt.MapFrom(src => Math.Round(src.nsegurocobertura, 2)))
                .ForMember(dest => dest.Dsegurofechavigencia, opt => opt.MapFrom(src => src.dsegurofechavigencia.ToString("yyy-MM-dd")));
            CreateMap<CreateTmseguroDto, TMSeguro>();

            //Afiliacion
            CreateMap<TRAfiliacion, GetTrafiliacion>()
                .ForMember(dest => dest.Cclientedocumentoidentidad, opt => opt.MapFrom(src => src.TMCliente.cclientedocumentoidentidad))
                .ForMember(dest => dest.Cseguronumero, opt => opt.MapFrom(src => src.TMSeguro.cseguronumero));

            CreateMap<TRAfiliacion, GetFullTrafiliacion>();
            CreateMap<CreateTrafiliacion, TRAfiliacion>();

            //Pago
            CreateMap<TRPago, GetTRPagoDto>()
                .ForMember(dest => dest.Dpagofecha, opt => opt.MapFrom(src => src.dpagofecha.ToString("dd/MM/yyyy")));
        }
    }
}