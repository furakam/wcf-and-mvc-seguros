﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WFCSeguro.Configs.AutoMapper;
using WFCSeguro.Model;
using WFCSeguro.Models.Dto.Seguro;

namespace WFCSeguro.Services
{
    public class SeguroService : ISeguroService
    {
        private IMapper mapper;

        public SeguroService()
        {
            mapper = AutoMapperConfig.GetMapper();
        }

        public Response<string> ActualizarSeguro(UpdateTmseguroDto seguroForm)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var seguroToUpdate = db.TMSeguro.Where(c => c.nseguroid == seguroForm.Nseguroid).FirstOrDefault() ?? throw new Exception($"El seguro con el id '{seguroForm.Nseguroid}' no fue encontrado");

                    mapper.Map(seguroForm, seguroToUpdate);
                    db.SaveChanges();
                }
                response.Data = "Actualizado con éxito";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        public Response<string> EliminarSeguro(int id)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var seguro = db.TMSeguro.Where(s => s.nseguroid == id).FirstOrDefault() ?? throw new Exception($"Seguro seguro con el id '{id}' no fue encontrado");
                    seguro.cseguroestado = false;
                    db.SaveChanges();
                }
                response.Data = "Eliminado con éxito";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        public Response<GetFullTmsegurodto> GetSeguroById(int id)
        {
            var response = new Response<GetFullTmsegurodto>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var seguro = db.TMSeguro.Where(s => s.nseguroid == id).FirstOrDefault() ?? throw new Exception("Seguro no encontrado");
                    response.Data = mapper.Map<GetFullTmsegurodto>(seguro);
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        public Response<IEnumerable<GetTmseguroDto>> ListarSeguro()
        {
            var response = new Response<IEnumerable<GetTmseguroDto>>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var seguros = db.TMSeguro.Where(s => s.cseguroestado == true).ToList();
                    response.Data = seguros.Select(c => mapper.Map<GetTmseguroDto>(c)).ToList();
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        public Response<string> RegistrarSeguro(CreateTmseguroDto seguroForm)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var formSeguro = mapper.Map<TMSeguro>(seguroForm);
                    db.TMSeguro.Add(formSeguro);
                    db.SaveChanges();
                }
                response.Data = "Registrado con éxito";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
    }
}
