﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WFCSeguro.Model;
using WFCSeguro.Models.Dto.Seguro;

namespace WFCSeguro.Services
{
    [ServiceContract]
    public interface ISeguroService
    {
        [OperationContract]
        Response<GetFullTmsegurodto> GetSeguroById(int id);

        [OperationContract]
        Response<IEnumerable<GetTmseguroDto>> ListarSeguro();

        [OperationContract]
        Response<string> RegistrarSeguro(CreateTmseguroDto seguroForm);

        [OperationContract]
        Response<string> ActualizarSeguro(UpdateTmseguroDto seguroForm);

        [OperationContract]
        Response<string> EliminarSeguro(int id);
        
    }
}
