﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WFCSeguro.Configs.AutoMapper;
using WFCSeguro.Model;
using WFCSeguro.Models.Dto.CompaniaAseguradora;

namespace WFCSeguro.Services.CompaniaAseguradora
{
    public class TMCompaniaAseguradoraService : ITMCompaniaAseguradoraService
    {
        private IMapper mapper;

        public TMCompaniaAseguradoraService()
        {
            mapper = AutoMapperConfig.GetMapper();
        }

        public Response<GetFullTmcompaniaAseguradoraDto> GetCompaiaAseguradoraById(int id)
        {
            var response = new Response<GetFullTmcompaniaAseguradoraDto>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var compania = db.TMCompaniaAseguradora.Where(c => c.ncompaniaid == id).FirstOrDefault() ?? throw new Exception("Compania no encontrada");
                    response.Data = mapper.Map<GetFullTmcompaniaAseguradoraDto>(compania);
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        public Response<List<GetTmcompaniaAseguradoraDto>> ListarCompaniaAseguradora()
        {
            var response = new Response<List<GetTmcompaniaAseguradoraDto>>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var companias = db.TMCompaniaAseguradora.Where(c => c.ccompaniaestado == true).ToList();
                    response.Data = companias.Select(c => mapper.Map<GetTmcompaniaAseguradoraDto>(c)).ToList();
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        public Response<string> RegistrarCompaniaAseguradora(CreateTmcompaniaAseguradoraDto companiaForm)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var formToCompania = mapper.Map<TMCompaniaAseguradora>(companiaForm);
                    db.TMCompaniaAseguradora.Add(formToCompania);
                    db.SaveChanges();
                }
                response.Data = "Registrado con éxito";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        public Response<string> ActualizarCompaniaAseguradora(UpdateTmcompaniaAseguradoraDto companiaForm)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var companiaToUpdate = db.TMCompaniaAseguradora.Where(c => c.ncompaniaid == companiaForm.Ncompaniaid).FirstOrDefault() ?? throw new Exception($"La compania con el id '{companiaForm.Ncompaniaid}' no fue encontrada");

                    mapper.Map(companiaForm, companiaToUpdate);
                    db.SaveChanges();
                }
                response.Data = "Actualizado con éxito";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        public Response<string> DeleteCompaiaAseguradoraById(int id)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var compania = db.TMCompaniaAseguradora.Where(c => c.ncompaniaid == id).FirstOrDefault() ?? throw new Exception($"La compania con el id '{id}' no fue encontrada");
                    compania.ccompaniaestado = false; 
                    db.SaveChanges();
                }
                response.Data = "Eliminado con éxito";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
    }
}
