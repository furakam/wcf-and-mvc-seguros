﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WFCSeguro.Model;
using WFCSeguro.Models.Dto.CompaniaAseguradora;

namespace WFCSeguro.Services.CompaniaAseguradora
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "ITMCompaniaAseguradoraService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ITMCompaniaAseguradoraService
    {
        [OperationContract]
        Response<List<GetTmcompaniaAseguradoraDto>> ListarCompaniaAseguradora();

        [OperationContract]
        Response<string> RegistrarCompaniaAseguradora(CreateTmcompaniaAseguradoraDto companiaForm);

        [OperationContract]
        Response<string> ActualizarCompaniaAseguradora(UpdateTmcompaniaAseguradoraDto companiaForm);

        [OperationContract]
        Response<GetFullTmcompaniaAseguradoraDto> GetCompaiaAseguradoraById(int id);

        [OperationContract]
        Response<string> DeleteCompaiaAseguradoraById(int id); 
    }
}
