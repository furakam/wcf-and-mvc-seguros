﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WFCSeguro.Configs.AutoMapper;
using WFCSeguro.Model;
using WFCSeguro.Models.Dto.Cliente;

namespace WFCSeguro.Services.Cliente
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ClienteService" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ClienteService.svc o ClienteService.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ClienteService : IClienteService
    {
        private IMapper mapper;

        public ClienteService()
        {
            mapper = AutoMapperConfig.GetMapper();
        }

        public Response<string> ActualizarCliente(UpdateTmclienteDto clienteForm)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var clienteToUpdate = db.TMCliente.Where(c => c.nclienteid == clienteForm.Nclienteid).FirstOrDefault() ?? throw new Exception($"El cliente con el id '{clienteForm.Nclienteid}' no fue encontrada");

                    mapper.Map(clienteForm, clienteToUpdate);
                    db.SaveChanges();
                }
                response.Data = "Actualizado con éxito";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        public Response<string> EliminarCliente(int id)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var cliente = db.TMCliente.Where(c => c.nclienteid == id).FirstOrDefault() ?? throw new Exception($"Cliente cliente con el id '{id}' no fue encontrada");
                    cliente.cclienteestado = false;
                    db.SaveChanges();
                }
                response.Data = "Eliminado con éxito";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        public Response<GetFullTmclienteDto> GetClienteById(int id)
        {
            var response = new Response<GetFullTmclienteDto>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var cliente = db.TMCliente.Where(c => c.nclienteid == id).FirstOrDefault() ?? throw new Exception("Cliente no encontrado");
                    response.Data = mapper.Map<GetFullTmclienteDto>(cliente);
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        public Response<IEnumerable<GetTmclienteDto>> ListarClientes()
        {
            var response = new Response<IEnumerable<GetTmclienteDto>>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var clientes = db.TMCliente.Where(c => c.cclienteestado == true).ToList();
                    response.Data = clientes.Select(c => mapper.Map<GetTmclienteDto>(c)).ToList();
                }
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }

        public Response<string> RegistrarCliente(CreateTmclienteDto clienteForm)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var formToCliente = mapper.Map<TMCliente>(clienteForm);
                    db.TMCliente.Add(formToCliente);
                    db.SaveChanges();
                }
                response.Data = "Registrado con éxito";
            }
            catch (Exception e)
            {
                response.Message = e.Message;
            }
            return response;
        }
    }
}
