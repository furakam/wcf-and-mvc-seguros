﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WFCSeguro.Model;
using WFCSeguro.Models.Dto.Cliente;

namespace WFCSeguro.Services.Cliente
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IClienteService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IClienteService
    {
        [OperationContract]
        Response<IEnumerable<GetTmclienteDto>> ListarClientes();

        [OperationContract]
        Response<GetFullTmclienteDto> GetClienteById(int id);

        [OperationContract]
        Response<string> RegistrarCliente(CreateTmclienteDto clienteForm);

        [OperationContract]
        Response<string> ActualizarCliente(UpdateTmclienteDto clienteForm);

        [OperationContract]
        Response<string> EliminarCliente(int id); 
         
    }
}
