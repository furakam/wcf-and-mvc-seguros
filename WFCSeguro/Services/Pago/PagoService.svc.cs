﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WFCSeguro.Configs.AutoMapper;
using WFCSeguro.Model;
using System.Data.Entity;
using WFCSeguro.Model.Dto.Pago;

namespace WFCSeguro.Services.Pago
{
    public class PagoService : IPagoService
    {
        private readonly IMapper mapper;

        public PagoService()
        {
            mapper = AutoMapperConfig.GetMapper();
        }

        private void VerificarCronogramaActivoExistente(int afiliacionId, BDSegurosEntities db)
        {
            var pago = db.TRPago.Where(p => p.nafiliacionid == afiliacionId).FirstOrDefault();
            if (pago != null) throw new Exception("Ya existe un cronograma generado");
        }

        public Response<string> GenerarCronogramaPago(int afiliacionId)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {

                    VerificarCronogramaActivoExistente(afiliacionId, db);

                    var afiliacion = db.TRAfiliacion.Include(a => a.TMSeguro).Where(a => a.nafiliacionid == afiliacionId).FirstOrDefault() ?? throw new Exception($"Afiliación con el id '{afiliacionId}' no encontrado");

                    for (int i = 1; i <= 12; i++)
                    {
                        DateTime fechaPago = afiliacion.dsegurofechaafiliación.Value.AddMonths(i);

                        var pago = new TRPago
                        {
                            nafiliacionid = afiliacion.nafiliacionid,
                            nclienteid = afiliacion.nclienteid,
                            nseguroid = afiliacion.nseguroid,
                            npagomes = (byte)fechaPago.Month,
                            npagoanio = fechaPago.Year,
                            dpagofecha = fechaPago,
                            npagocuota = afiliacion.TMSeguro.nseguroimportemensual
                        };
                        db.TRPago.Add(pago);
                    }
                    db.SaveChanges();
                    response.Data = "Generación de pagos exitosa";
                }
            }
            catch (Exception error)
            {
                response.Message = error.Message;
            }
            return response;
        }

        public Response<IEnumerable<GetTRPagoDto>> GetPagosByAfiliacionId(int afiliaciónId)
        {
            var response = new Response<IEnumerable<GetTRPagoDto>>();
            try
            {
                using(var db = new BDSegurosEntities())
                {
                    var pagos = db.TRPago.Where(a => a.nafiliacionid == afiliaciónId).ToList();
                    response.Data = pagos.Select(p => mapper.Map<GetTRPagoDto>(p));
                }
            }
            catch (Exception error)
            {
                response.Message = error.Message;
            }
            return response; 
        }
    }
}
