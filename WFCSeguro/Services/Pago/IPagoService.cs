﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WFCSeguro.Model;
using WFCSeguro.Model.Dto.Pago;

namespace WFCSeguro.Services.Pago
{
    [ServiceContract]
    public interface IPagoService
    {
        [OperationContract]
        Response<string> GenerarCronogramaPago(int afiliacionId);

        [OperationContract]
        Response<IEnumerable<GetTRPagoDto>> GetPagosByAfiliacionId(int afiliaciónId);
    }
}
