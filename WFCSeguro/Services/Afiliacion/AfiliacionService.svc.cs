﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WFCSeguro.Configs.AutoMapper;
using WFCSeguro.Model;
using WFCSeguro.Models.Dto.Afiliacion;
using System.Data.Entity;

namespace WFCSeguro.Services.Afiliacion
{
    public class AfiliacionService : IAfiliacionService
    {
        private readonly IMapper mapper;

        public AfiliacionService()
        {
            mapper = AutoMapperConfig.GetMapper();
        }



        private void VerificarEdad(int seguroId, int clienteId, BDSegurosEntities db)
        {
            var seguro = db.TMSeguro.Where(s => s.nseguroid == seguroId).FirstOrDefault() ?? throw new Exception("Seguro no encontrado");

            var cliente = db.TMCliente.Where(c => c.nclienteid == clienteId).FirstOrDefault() ?? throw new Exception("Cliente no encontrado");

            TimeSpan edad = DateTime.Today - cliente.dclientefechanacimiento;
            int edadEnAnios = (int)edad.TotalDays / 365;

            if (edadEnAnios > seguro.nseguroedadmaxima) throw new Exception("Supera la edad máxima");
        }

        public Response<string> ActualizarAfiliacion(UpdateTrafiliacionDto afiliacionForm)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var afiliacionToUpdate = db.TRAfiliacion.Where(a => a.nafiliacionid == afiliacionForm.Nafiliacionid).FirstOrDefault() ?? throw new Exception($"Afiliación con el id {afiliacionForm.Nafiliacionid}");

                    VerificarEdad(afiliacionForm.Nseguroid, afiliacionForm.Nclienteid, db);

                    mapper.Map(afiliacionForm, afiliacionToUpdate);
                    db.SaveChanges();

                    response.Data = "Actualizado con éxito";
                }
            }
            catch (Exception error)
            {
                response.Message = error.Message;
            }
            return response;
        }

        public Response<string> EliminarAfiliacion(int id)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var afiliacion = db.TRAfiliacion.Where(a => a.nafiliacionid == id).FirstOrDefault() ?? throw new Exception($"Afiliación con el id {id}");
                    afiliacion.cafiliacionestado = false;
                    db.SaveChanges();
                    response.Data = "Eliminado con exito";
                }
            }
            catch (Exception error)
            {
                response.Message = error.Message;
            }
            return response;
        }

        public Response<GetFullTrafiliacion> GetAfiliacionById(int id)
        {
            var response = new Response<GetFullTrafiliacion>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var afiliacion = db.TRAfiliacion.Where(a => a.cafiliacionestado == true && a.nafiliacionid == id).FirstOrDefault() ?? throw new Exception("Afiliación no encontrada");
                    response.Data = mapper.Map<GetFullTrafiliacion>(afiliacion);
                }
            }
            catch (Exception error)
            {
                response.Message = error.Message;
            }
            return response;
        }

        public Response<IEnumerable<GetTrafiliacion>> ListarAfiliacion()
        {
            var response = new Response<IEnumerable<GetTrafiliacion>>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    var afiliaciones = db.TRAfiliacion.Include(a => a.TMCliente).Include(a => a.TMSeguro).Where(a => a.cafiliacionestado == true).ToList();

                    response.Data = afiliaciones.Select(a =>
                    {
                        var obj = mapper.Map<GetTrafiliacion>(a);
                        obj.Cronograma = db.TRPago.Any(p => p.nafiliacionid == a.nafiliacionid);
                        return obj;
                    }).ToList();
                }
            }
            catch (Exception error)
            {
                response.Message = error.Message;
            }
            return response;
        }

        public Response<string> RegistrarAfiliacion(CreateTrafiliacion afiliacionForm)
        {
            var response = new Response<string>();
            try
            {
                using (var db = new BDSegurosEntities())
                {
                    VerificarEdad(afiliacionForm.Nseguroid, afiliacionForm.Nclienteid, db);

                    var formAfiliacion = mapper.Map<TRAfiliacion>(afiliacionForm);
                    db.TRAfiliacion.Add(formAfiliacion);
                    db.SaveChanges();

                    response.Data = "Registrado con éxito";
                }
            }
            catch (Exception error)
            {
                response.Message = error.Message;
            }
            return response;
        }
    }
}
