﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WFCSeguro.Model;
using WFCSeguro.Models.Dto.Afiliacion;

namespace WFCSeguro.Services.Afiliacion
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IAfiliacionService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IAfiliacionService
    {
        [OperationContract]
        Response<GetFullTrafiliacion> GetAfiliacionById(int id);

        [OperationContract]
        Response<IEnumerable<GetTrafiliacion>> ListarAfiliacion();

        [OperationContract]
        Response<string> RegistrarAfiliacion(CreateTrafiliacion afiliacionForm);

        [OperationContract]
        Response<string> ActualizarAfiliacion(UpdateTrafiliacionDto afiliacionForm);

        [OperationContract]
        Response<string> EliminarAfiliacion(int id);
    }
}
